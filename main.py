import logging as log
import time
from datetime import datetime
from itertools import combinations

import requests
import syllables
import whois
from dns.resolver import NXDOMAIN, resolve

log.basicConfig(level=log.INFO)


class DomainNameGenerator:
    def __init__(self, words_url, domain_extension='.com', minimum_word_length=3, syllables_per_word=1, words_per_domain=2):
        self.words_url = words_url
        self.words_file_name = words_url.split('/')[-1]
        self.minimum_word_length = minimum_word_length
        self.syllables_per_word = syllables_per_word
        self.words_per_domain = words_per_domain
        self.domain_extension = domain_extension
        self.raw_words = ''
        self.words = self.domains = self.word_combinations = []

    def get_words_from_url(self):
        r = requests.get(self.words_url).text
        self.raw_words = r

    def write_words_to_file(self):
        with open(self.words_file_name, 'w') as f:
            f.write(self.raw_words)
        log.info(f'Saved words to {self.words_file_name}')

    def open_words_from_file(self):
        with open(self.words_file_name, 'r') as f:
            self.raw_words = f.read()
        # print(self.words_file.read(100))
        print(self.raw_words[:100])
        # self.words_file.seek(0)
        log.info(f'Opened {self.words_file_name}')

    def parse_words_file(self):
        # self.words = [line.rstrip() for line in self.raw_words]
        self.words = self.raw_words.split('\n')

    def filter_words(self):
        if self.minimum_word_length:
            self.words = [word for word in self.words if len(word) >= self.minimum_word_length]
        if self.syllables_per_word:
            self.words = [word for word in self.words if syllables.estimate(word) == self.syllables_per_word]

    def build_word_combinations(self):
        self.word_combinations = combinations(self.words, self.words_per_domain)

    def build_domains(self):
        self.domains = [combination[0] + combination[1] + self.domain_extension for combination in self.word_combinations]

    def generate(self, get_from_file=False, write_to_file=False):
        if get_from_file:
            self.open_words_from_file()
        else:
            self.get_words_from_url()
            if write_to_file:
                self.write_words_to_file()

        self.parse_words_file()
        self.filter_words()
        print(f'{str(len(self.words))} words.')
        log.debug(self.words[:10])
        self.build_word_combinations()
        self.build_domains()
        print(f'{str(len(self.domains))} domains generated.')
        log.debug(self.domains[:10])


class Domain:
    def __init__(self, name):
        self.name = name
        self.is_available = None
        self.dns_succeeded = None
        self.whois_succeeded = None

    def check_dns(self):
        log.debug(f'DNS {self.name}')
        try:
            resolve(domain_name, 'A')  # Looks for A records.
        except NXDOMAIN:
            self.dns_succeeded = True
            self.is_available = True
        except Exception as e:
            log.error(f'Error performing DNS lookup on {self.name}: {e}')
            self.dns_succeeded = False
        else:
            self.dns_succeeded = True
            self.is_available = False

    def check_whois(self):
        log.debug(f'Whois {self.name}')
        try:
            whois_result = whois.query(self.name)
        except Exception as e:
            log.error(f'Error performing Whois lookup on {self.name}: {e}')
            self.whois_succeeded = False
        else:
            self.whois_succeeded = True
            if whois_result:
                self.is_available = False
            else:
                self.is_available = True

    def write_result(self, available_domain_file, unavailable_domain_file, whois_errors_file):
        if self.is_available:
            available_domain_file.write(f'{self.name}\n')
        elif not self.whois_succeeded:
            whois_errors_file.write(f'{domain_name}\n')
        elif not self.is_available:
            unavailable_domain_file.write(f'{self.name}\n')
        else:
            log.error(f'Something went wrong with {self.name}')

    def check_availability(self):
        self.check_dns()
        if not self.dns_succeeded:
            self.check_whois()  # Use slower Whois when DNS fails


if __name__ == '__main__':
    # short_words_url = 'https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-usa-no-swears-short.txt'
    all_words_url = 'https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-usa-no-swears.txt'
    domain_name_generator = DomainNameGenerator(words_url=all_words_url)
    domain_name_generator.generate(get_from_file=False, write_to_file=True)

    start_time = time.time()
    date_now = datetime.now().strftime("%Y%m%d")
    with open(f'domain_names_available_{date_now}.csv', 'w') as f_available:
        with open(f'domain_names_unavailable_{date_now}.csv', 'w') as f_unavailable:
            with open(f'domain_names_whois_errors_{date_now}.csv', 'w') as f_whois_errors:
                for domain_name in domain_name_generator.domains[:2000]:
                    domain = Domain(domain_name)
                    domain.check_availability()
                    domain.write_result(f_unavailable, f_unavailable, f_whois_errors)
    end_time = time.time()

    # total_count = available_count = unavailable_count = error_count = 0
    # print(f'{str(total_count)} total domains checked.')
    # print(f'{str(available_count)} available domains.')
    # print(f'{str(unavailable_count)} unavailable domains.')
    # print(f'{str(error_count)} errors.')
    print(f'{str(end_time - start_time)} seconds taken.')
